# README #

EasyPop v3; a comprehensive lightbox plugin.

Whether you're looking to responsively present text, images, videos, embeds or HTML; there is a box type to suit.

Also featuers alert (info/success/warning/error), prompt and confirm boxes.

### Latest Release: 3.2.0

### Documentation ###

https://shane-matthews.com/documentation/easypop

### Browser Compatibility ###

* Chrome
* Firefox
* Edge
* Safari
* IE9+
* Pretty much anything from this age, mobile included