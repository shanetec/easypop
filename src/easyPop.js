﻿/*!
 * EasyPop; for easy and pretty light-boxes
 * 3.2.0
 * shane-matthews.com/documentation/easypop
 */

// ##################################
//           master helper
// ##################################

var EasyPop = {

	version: '3.2.0',

	// box types
	Type: {
		Standard: 'standard',
		Media: 'media',
		Error: 'interrupt-error',
		Success: 'interrupt-success',
		Warning: 'interrupt-warning',
		Info: 'interrupt-info',
		Prompt: 'action-prompt',
		Buttons: 'action-buttons'
	},

	// media types
	MediaType: {
		Auto: 'auto',
		Image: 'image',
		Video: 'video',
		Youtube: 'youtube',
		Vimeo: 'vimeo',
		Frame: 'frame'
	},

	// magnify port types
	MagnifyType: {
		Port: 'port', // little box that follows the cursor around
		Full: 'full' // entire box is magnified around cursor point
	},

	// magnification on/off modes
	MagnifyMode: {
		None: 'none',
		Toggle: 'toggle',
		Forced: 'forced'
	},

	// close button positioning
	CloseMode: {
		Inside: 'enabled-inside',
		Outside: 'enabled-outside',
		None: 'disabled'
	},

	// box states
	State: {
		Hidden: 'hidden',
		Normal: 'normal',
		Loading: 'loading',
		Destroyed: 'destroyed',
		Broken: 'broken'
	},

	// messages shown incrementally when loading media
	LoadingHints: [
		'Loading',
		'Still loading',
		'Still loading,<br/>please wait',
		'Loading is taking longer than usual,<br/>there may be network issues.'
	],

	// box fade-in effects
	Effect: {
		None: 'none',
		Grow: 'grow',
		FlipDown: 'flip-down',
		SlideDown: 'slide-down',
		DropBounce: 'drop-bounce',
		GrowBounce: 'grow-bounce'
	},

	// box/master events
	Event: {
		Blur: 'ep-blur',
		Focus: 'ep-focus',
		Init: 'ep-init',
		Create: 'ep-create',
		Show: 'ep-show',
		Hide: 'ep-hide',
		Move: 'ep-move',
		ContentChange: 'ep-content-change',
		StateChange: 'ep-state-change',
		TargetChange: 'ep-target-change'
	},

	// video play types
	PlayType: {
		Immediate: 'immediate',
		CanPlayThrough: 'can-play-through',
		FullLoad: 'full-load'
	},

	// internal tracking object, don't ever use in production
	_track: {

		id: 0,
		boxes: {},
		shown: [],
		target: null,
		isShown: false,
		container: null,
		initialised: false,
		topmostIndex: 5,
		loadingHintIndex: 0,
		blacklistedHeadHosts: [],
		lastMagnifyPoint: {
			x: 0,
			y: 0
		},

		timers: {
			throttleWindowResize: null,
			loadingUpdate: null,
		},

	},

	// config values, ideally you'd set these via EasyPop.config(...) for future-proofing
	_config: {
		defaultMediaWidth: 1280,
		defaultMediaHeight: 720,
		resizeMediaToPort: true,
		portSpaceAvailable: .9,
		windowResizeThrottleMs: 40,
		removeMediaOnHide: true,
		youtubeParameters: {
			'autoplay': 1,
			'rel': 0,
			'iv_load_policy': 3,
			'color': 'white',
			'showinfo': 0
		},
		vimeoParameters: {
			'autoplay': 1,
			'byline': 0,
			'portrait': 0,
			'title': 0,
			'transparent': 0
		},
		loadingProgressDelay: 3000,
		enforceMediaBoxMinSize: true,
		mediaBoxMinSize: {
			width: 150,
			height: 80
		},
		headRequestOnMedia: true,
		magnifyToggleMinRatio: 1.25,
		defaultEffect: 'grow',
		debugEvents: false

	},

	config: function (properties) {
		if (properties != null && typeof properties === 'object')
			for (var o in properties)
				if (properties.hasOwnProperty(o))
					if (typeof this._config[o] !== 'undefined') {
						if (properties[o] != null && typeof this._config[o] === typeof properties[o])
							this._config[o] = properties[o];
						else
							console.warn("EasyPop: Value for config property \"" + o + "\" is not an accepted type");
					} else
						console.warn("EasyPop: Invalid property (\"" + o + "\")", properties);
	},

	create: function (id, properties) {
		return new EasyPopBox(id, properties);
	},

	get: function (id) {
		return this._track.boxes[id];
	},

	_add: function (id, box) {
		this._track.boxes[id] = box;
		this._track.container.append(box.element);
	},

	_remove: function (id) {
		if (this._track.shown.indexOf(id) !== -1)
			this._track.shown.splice(this._track.shown.indexOf(id), 1);
		delete this._track.boxes[id];
	},

	getShown: function () {
		return this._track.shown.map(function (id) {
			return ep._track.boxes[id];
		});
	},

	setTarget: function (element) {

		var oldTarget = this._track.target;

		this._track.target = element instanceof window.jQuery ? element : $(element);

		if (this._track.container)
			this._track.container.appendTo(this._track.target);

		this._dispatch(this.Event.TargetChange, {
			target: this._track.target,
			replaced: oldTarget
		});

	},

	isInitialised: function () {
		return this._track.initialised;
	},

	_show: function (id) {

		if (this._track.shown.length === 0) // no boxes shown yet; show the blackout too
			this._track.container.show().find('#ep-blackout').fadeIn(150);

		this._track.isShown = true;
		this._track.shown.push(id.toString());

	},

	_hide: function (id) {

		if (id == null)
			id = this._track.shown[this._track.shown.length - 1];

		this._track.shown.splice(this._track.shown.indexOf(id), 1);

		// no more boxes shown; hide the blackout too
		if (ep._track.shown.length === 0) {

			this._track.isShown = false;

			ep._track.container.find('#ep-blackout').fadeOut(150, 'linear', function () {
				ep._track.container.hide();
			});

		} else
			this.setTopmost(this._track.shown[this._track.shown.length - 1]);

	},

	getTopmost: function () {

		if (!this._track.shown.length)
			throw new Error("Cannot getTopmost; no boxes shown");

		return this._track.boxes[this._track.shown[this._track.shown.length - 1]];

	},

	setTopmost: function (id) {

		var index = this._track.shown.indexOf(id.toString());
		if (index === -1)
			throw new Error("EasyPop: Cannot setTopmost; box with id '" + id + "' is not shown");

		var newTopmost = this._track.boxes[id];

		this._track.container.find('.ep-window').attr({ 'data-ep-topmost': false });
		newTopmost.element.attr({ 'data-ep-topmost': true }).css({ zIndex: ++this._track.topmostIndex });

		// shown array is bottom-most on the left to top-most on the right
		var oldTopmost = ep.get(this._track.shown[this._track.shown.length - 1]);

		if (index !== this._track.shown.length - 1)
			this._track.shown.splice(this._track.shown.length - 1, 0, this._track.shown.splice(index, 1)[0]);

		this._dispatch(this.Event.Blur, {
			box: oldTopmost,
			focused: newTopmost
		});

		this._dispatch(this.Event.Focus, {
			box: newTopmost,
			blurred: oldTopmost
		});

	},

	_toggleLoader: function () {

		var anyLoading = this._track.shown.some(function (id) {
			return ep._track.boxes[id].state === ep.State.Loading;
		});

		if (anyLoading)
			this._track.container.find('#ep-loading').fadeIn(150);
		else
			this._track.container.find('#ep-loading').fadeOut(150);

	},

	_dispatch: function (event, params) {

		//if (typeof this.Event[event] === 'undefined')
		//	throw new RangeError("EasyPop: Failed to dispatch event; invalid event \"" + event + "\"");

		if (this._config.debugEvents === true)
			console.debug("EasyPop: Event \"" + event + "\"", params);

		if (params && params.box instanceof EasyPopBox)
			params.box.element.trigger(event, params);
		else
			$(window).trigger(event, params || {});

	},

	// ##################################
	//          init / bindings
	// ##################################

	init: function () {

		if (!this._track.target)
			this.setTarget(document.body);

		// create container
		var container = $('<div id=ep-container>');

		container.appendTo(this._track.target);
		this._track.container = container;

		// create blackout
		var blackout = $('<div id=ep-blackout>');
		blackout.appendTo(this._track.container);

		blackout.on('click tap', function () {

			var topBox = ep.get(ep._track.shown[ep._track.shown.length - 1]);

			if (topBox.closeMode !== ep.CloseMode.None)
				topBox.hide();

		});

		// loader
		var loader = $('<div id=ep-loading><output id=ep-progress></output></div>');
		loader.appendTo(this._track.container);

		this._track.initialised = true;

		// bindings
		$(window).on('resize', function () {

			clearTimeout(ep._track.timers.throttleWindowResize);

			ep._track.timers.throttleWindowResize = setTimeout(function () {

				var shown = ep.getShown();

				for (var i in shown)
					if (shown.hasOwnProperty(i))
						shown[i].reposition();

			}, ep._config.windowResizeThrottleMs);

		});

		// control gallery with left/right arrows
		$(window).on('keydown', function (e) {

			if (!ep._track.isShown)
				return;

			var box = ep.getTopmost();

			if (box.type === ep.Type.Media && box.mediaArray != null)
				if (e.keyCode === 37)
					box.element.find(".ep-gallery-arrow[data-ep-direction='left']").trigger('click');
				else if (e.keyCode === 39)
					box.element.find(".ep-gallery-arrow[data-ep-direction='right']").trigger('click');

		});

		// close buttons
		container.on('click tap', '[data-ep-close]', function (e) {
			e.preventDefault();

			var box = $(this).parents('.ep-window');
			ep.get(box.attr('data-ep-id')).hide();
		});

		// topmost
		container.on('click tap', '.ep-window[data-ep-topmost=false]', function (e) {
			e.preventDefault();

			if (typeof $(e.target).attr('data-ep-close') !== 'undefined')
				return;

			ep.setTopmost($(this).attr('data-ep-id'));
		});

		// buttons
		container.on('click', 'button.ep-button', function () {

			var id = $(this).attr('data-ep-button-id'),
				boxId = $(this).parents('.ep-window').attr('data-ep-id'),
				box = ep.get(boxId),
				value = null;

			if (box.type === ep.Type.Prompt)
				value = box.element.find('input.ep-input').val();

			if (typeof box.buttons[id].click === 'function') {
				if (box.buttons[id].click(value) !== false)
					box.hide();
			}
			else
				box.hide();

		});

		// gallery arrows
		container.on('click tap', 'button.ep-gallery-arrow', function (e) {

			e.preventDefault();

			var boxId = $(this).closest('.ep-window').attr('data-ep-id'),
				box = ep.get(boxId);

			box.mediaIndex = ($(this).attr('data-ep-direction') === 'left' ? '-' : '+') + '=1';

		});

		// prompts
		container.on('keypress', '.ep-input', function (e) {

			if (e.keyCode !== 13)
				return;

			var boxId = $(this).closest('.ep-window').attr('data-ep-id'),
				box = ep.get(boxId);

			if (box.type === ep.Type.Prompt) {
				e.preventDefault();
				box.element.find('.ep-buttons').find('[data-ep-default-button]').trigger('click');
			}

		});

		// toggle magnify port
		container.on('click', 'button.ep-magnify-toggle', function () {
			var box = ep.get($(this).parents('.ep-window').attr('data-ep-id'));
			box.magnify = !box.magnify;
		});

		// adjust magnify port on mouse/touch move
		// we use vanilla js wherever possible due to this being a high frequency event, particularly problematic on IE/Edge
		container.on('mousemove touchmove', '.ep-window[data-ep-magnify]', function (e) {

			var // touch events are encapsulated
				oe = e.type === 'touchmove' ? e.originalEvent.touches[0] : e,
				isRealEvent = typeof oe.pageX !== 'undefined',

				box = ep.get(this.getAttribute('data-ep-id')),
				media = this.getElementsByClassName('ep-media')[0],
				port = this.getElementsByClassName('ep-magnify-port')[0],

				isPort = box.magnifyType === ep.MagnifyType.Port,

				bounds = media.getBoundingClientRect(),
				width = bounds.width,
				height = bounds.height,

				scrollY = (window.pageYOffset || document.documentElement.scrollTop) - (document.documentElement.clientTop || 0),

				// mouse coords, limited to the outer bounds of the box
				mouseX = Math.min(Math.max((isRealEvent ? oe.pageX : ep._track.lastMagnifyPoint.x) - bounds.left, 0), width),
				mouseY = Math.min(Math.max((isRealEvent ? oe.pageY - scrollY : ep._track.lastMagnifyPoint.y) - bounds.top, 0), height),

				// ratio of the source image to box bounds
				ratio = ((box.media.width || ep._config.defaultMediaWidth) / width) * box._magnifyZoomRatio,

				// background coords
				positionX,
				positionY;

			if (isPort) {
				positionX = (-Math.min(Math.max((isRealEvent ? oe.pageX : ep._track.lastMagnifyPoint.x) - bounds.left, 0), width) * ratio) + (box._magnifyPortWidth / 2);
				positionY = (-Math.min(Math.max((isRealEvent ? oe.pageY - scrollY : ep._track.lastMagnifyPoint.y) - bounds.top, 0), height) * ratio) + (box._magnifyPortHeight / 2);
			}
			else {
				positionX = Math.max(Math.min(-((mouseX * ratio) - (box._magnifyPortWidth / 2)), isPort ? width : 0), -((width * ratio) - width));
				positionY = Math.max(Math.min(-((mouseY * ratio) - (box._magnifyPortHeight / 2)), isPort ? height : 0), -((height * ratio) - height));
			}

			port.style.left = (isPort ? mouseX - (box._magnifyPortWidth / 2) : 0) + 'px';
			port.style.top = (isPort ? mouseY - (box._magnifyPortHeight / 2) : 0) + 'px';
			port.style.backgroundPosition = positionX + 'px ' + positionY + 'px';
			port.style.backgroundSize = ((box.media.width || ep._config.defaultMediaWidth) * box._magnifyZoomRatio) + 'px ' + ((box.media.height || ep._config.defaultMediaHeight) * box._magnifyZoomRatio) + 'px';

			if (isRealEvent)
				ep._track.lastMagnifyPoint = {
					x: oe.pageX,
					y: oe.pageY
				};

		});

		// hide magnify port on click
		container.on('click', '.ep-magnify-port', function () {
			var box = ep.get($(this).parents('.ep-window').attr('data-ep-id'));
			box.magnify = false;
		});

		// zoom in/out when magnifying
		container.on('mousewheel', '.ep-magnify-port', function (e) {

			var delta = Math.max(-1, Math.min(1, (e.originalEvent.wheelDelta || -e.originalEvent.detail))),
				box = ep.get($(this).parents('.ep-window').attr('data-ep-id')),
				media = box.element.find('.ep-media'),
				boxRatio = media.width() / (box.media.width || ep._config.defaultMediaWidth);

			box._magnifyZoomRatio = Math.min(Math.max(box._magnifyZoomRatio + (delta > 0 ? .1 : -.1), boxRatio), 1);

			box.element.trigger('mousemove');

		});

		this._dispatch(this.Event.Init);

	},

};

var ep = EasyPop;

// ##################################
//              Presets
// ##################################

ep.preset = {

	// preset for a buttons box with cancel/confirm buttons
	// essentially overloaded, allowing for an invoking DOM object to be passed with or without onSuccess/onFail arguments
	confirm: function (title, content, onSuccessOrInvoker, onFail, invoker) {

		var invokerArgument = typeof invoker !== 'undefined' && invoker instanceof HTMLElement ? invoker : (typeof onSuccessOrInvoker !== 'undefined' && onSuccessOrInvoker instanceof HTMLElement ? onSuccessOrInvoker : null);

		// check if the confirm button was clicked
		if (invokerArgument && $(invokerArgument).data('_ep-bounce') === true) {
			$(invokerArgument).data({ '_ep-bounce': null }); // allows the prompt to be shown again, in case we're not actually changing location
			return true;
		}

		var box = new EasyPopBox(null, {
			type: ep.Type.Standard,
			title: title || 'Are you sure?',
			content: content,
			buttons: [
				{
					label: 'Cancel',
					color: 'white',
					click: onFail
				},
				{
					label: 'Confirm',
					color: 'blue',
					click: function (args) {

						// if an onSuccess function was provided, execute it too, with the same arguments as the current scope
						if (typeof onSuccessOrInvoker === 'function')
							onSuccessOrInvoker.apply(this, args);

						if (invokerArgument)
							$(invokerArgument).data({ '_ep-bounce': true }).get(0).click(); // set bounce data as a means of checking what type of click this is

					},
					bindEnterKey: true
				}
			]
		});

		return invokerArgument ? false : box;

	},

};

// ##################################
//             EasyPopBox
// ##################################

var EasyPopBox = function (id, properties) {

	if (!ep.isInitialised())
		ep.init();

	this.id = id != null ? id : ++ep._track.id;
	this.closeMode = ep.CloseMode.Inside;
	this.showOnCreate = true;
	this.overwrite = true;
	this.media = null;
	this.mediaArray = null;
	this.magnifyMode = ep.MagnifyMode.Toggle;
	this.magnifyType = ep.MagnifyType.Full;
	this.effect = null;

	this.onSuccess = null;
	this.onFail = null;

	this._title = null;
	this._content = null;
	this._buttons = [];
	this._type = ep.Type.Standard;
	this._state = ep.State.Hidden;
	this._lastMediaType = null;
	this._magnify = null;
	this._magnifyPortWidth = null;
	this._magnifyPortHeight = null;
	this._magnifyZoomRatio = 1.0;
	this._mediaIndex = null;

	if (this.overwrite && ep.get(this.id))
		ep.get(this.id).destroy();

	this.element = $("<div id='ep-window-" + this.id + "' data-ep-id='" + this.id + "'><div class='ep-title'></div></div>");
	this.element[0].className = 'ep-window';

	ep._add(this.id, this);

	var updated = [],
		// fields in the queue will be updated after all others in the properties param, also guarantees setter invoke
		queued = ['title', 'content'];

	if (properties != null && typeof properties === 'object') {

		// ensure type is set before anything else
		this.type = typeof properties.type !== 'undefined' ? properties.type : this._type;
		updated.push('type');

		for (var o in properties)
			if (properties.hasOwnProperty(o) && updated.indexOf(o) === -1)
				if (typeof this[o] !== 'undefined') {
					if (properties[o] != null) {
						if (queued.indexOf(o) !== -1)
							this['_' + o] = properties[o];
						else
							this[o] = properties[o];
						updated.push(o);
					}
				} else
					console.warn("EasyPop: Invalid property (\"" + o + "\")", properties);
	}
	else
		this.type = this._type;

	for (var n in queued)
		if (queued.hasOwnProperty(n))
			this[queued[n]] = this['_' + queued[n]];

	this.element[0].setAttribute('data-ep-effect', this.effect || ep._config.defaultEffect);

	ep._dispatch(ep.Event.Create, {
		box: this
	});

	if (this.showOnCreate)
		this.show();

};

// EasyPopBox.title
Object.defineProperty(EasyPopBox.prototype, "title", {

	get: function title() {
		return this._title;
	},

	set: function title(value) {

		this._title = value || '';

		var titleElement = this.element.find('.ep-title');

		titleElement.html("<span class='ep-text'>" + this.title + "</span>");

		this.element.find('.ep-close').remove();

		if (this.closeMode !== ep.CloseMode.None) {
			var closeElement = $("<span class='ep-close' data-ep-close></span>");
			closeElement.prependTo(this.closeMode === ep.CloseMode.Inside && this.title && !this.media ? titleElement : this.element);
		}

		titleElement.toggle(!!this.title);

		this.reposition();

	}

});

// EasyPopBox.content
Object.defineProperty(EasyPopBox.prototype, "content", {

	get: function content() {
		return this._content;
	},

	set: function content(value) {

		this._content = (typeof value === 'undefined' ? this._content : value) || '';

		if (!this.element.find('.ep-content').length)
			this.element.append("<div class='ep-content'></div>");

		var contentElement = this.element.find('.ep-content'),
			prepend = "",
			append = "";

		if ([ep.Type.Success, ep.Type.Info, ep.Type.Error, ep.Type.Warning].indexOf(this.type) !== -1)
			prepend = "<span class='ep-icon ep-icon-" + this.type.replace('interrupt-', '') + "' aria-label='" + this.type.replace('interrupt-', '') + "'></span>";

		// prompt field
		if (this.type === ep.Type.Prompt) {
			append += "<div class='ep-field'><input type='text' class='ep-input' placeholder='Enter text here...' /></div>";

			if (!this.buttons || !this.buttons.length) {
				this.buttons = [
					{
						label: 'Cancel',
						color: 'white',
						click: this.onFail
					},
					{
						label: 'Submit',
						color: 'blue',
						click: this.onSuccess,
						bindEnterKey: true
					}
				];
			}
		}

		contentElement.html(prepend + "<span class='ep-text'>" + (value instanceof window.jQuery ? '' : this._content) + "</span>");

		if (value instanceof window.jQuery)
			value.appendTo(contentElement.find('.ep-text')).removeAttr('hidden');

		append && this.element.append(append);

		this.buttons = this._buttons;

		ep._dispatch(ep.Event.ContentChange, {
			box: this
		});

	}

});

// EasyPopBox.mediaIndex
Object.defineProperty(EasyPopBox.prototype, "mediaIndex", {

	get: function mediaIndex() {
		return this._mediaIndex;
	},

	set: function mediaIndex(value) {

		var calculatedIndex = value;

		if (typeof value === 'string' && value.length > 2)
			if (value.charAt(0) === '+')
				calculatedIndex = (this._mediaIndex || 0) + Number(value.replace(/^\+(?:=|)/, ''));
			else if (value.charAt(0) === '-')
				calculatedIndex = (this._mediaIndex || 0) - Number(value.replace(/^\-(?:=|)/, ''));

		this._mediaIndex = calculatedIndex;

		if (ep._track.shown.indexOf(this.id.toString()) !== -1)
			this.setMedia(this.mediaArray, calculatedIndex);

	}

});

// EasyPopBox.state
Object.defineProperty(EasyPopBox.prototype, "state", {

	get: function state() {
		return this._state;
	},

	set: function state(value) {

		var inst = this;
		var oldState = this._state;

		this._state = value;

		ep._track.container.find('#ep-loading').find('#ep-progress').html('');

		if (oldState === ep.State.Loading && (value === ep.State.Normal || value === ep.State.Broken))
			setTimeout(function () {
				inst.element.attr({ 'data-ep-state': value });
				inst.reposition();
			}, 300);
		else
			this.element.attr({ 'data-ep-state': value });

		ep._dispatch(ep.Event.StateChange, {
			box: this,
			state: this._state,
			replaced: oldState
		});

		ep._toggleLoader();

	}

});

// EasyPopBox.type
Object.defineProperty(EasyPopBox.prototype, "type", {

	get: function type() {
		return this._type;
	},

	set: function type(value) {

		this._type = value;

		this.element.attr({ 'data-ep-type': value });

	}

});

// EasyPopBox.magnify
Object.defineProperty(EasyPopBox.prototype, "magnify", {

	get: function magnify() {
		return this._magnify;
	},

	set: function magnify(value) {

		if (typeof value !== 'boolean')
			throw new TypeError("EasyPopBox.magnify expects a boolean value.");

		this._magnify = value;

		if (value) {

			this.element.attr({ 'data-ep-magnify': this.magnifyType });

			var container = $("<div class='ep-magnify-container'></div>"),
				port = $("<div class='ep-magnify-port' style='" + (this.magnifyType === ep.MagnifyType.Full ? 'width:100%;height:100%;' : '') + "'></div>");

			port.css({ backgroundImage: "url('" + (this.media.magnifySrc || this.media.src) + "')" });

			port.appendTo(container);
			container.appendTo(this.element);

			this._magnifyPortWidth = port.width();
			this._magnifyPortHeight = port.height();

		} else {
			this.element.removeAttr('data-ep-magnify');
			this.element.find('.ep-magnify-port').remove();
		}

		this.element.find('.ep-magnify-toggle').toggleClass('ep-active', value);

	}

});

// EasyPopBox.buttons
Object.defineProperty(EasyPopBox.prototype, "buttons", {

	get: function buttons() {
		return this._buttons;
	},

	set: function buttons(value) {

		if (value != null && !Array.isArray(value))
			throw new TypeError("EasyPopBox.buttons expects an Array value.");

		this._buttons = value || [];

		// remove existing footer
		var existingFooter = this.element.find('footer.ep-buttons');
		existingFooter.length && existingFooter.remove();

		// build footer
		if (this._buttons.length) {

			var append = "<footer class='ep-buttons'>";

			for (var b in this._buttons)
				if (this._buttons.hasOwnProperty(b))
					append += "<button type='button' class='ep-button ep-button-" + this._buttons[b].color.replace('#', '') + "' data-ep-button-id='" + b + "'" + (this._buttons[b].color.charAt(0) === '#' ? " style='background-color:" + this._buttons[b].color + "'" : '') + (this._buttons[b].bindEnterKey === true ? " data-ep-default-button" : "") + ">" + this._buttons[b].label + "</button>";

			this.element.append(append + "</footer>");

		}

	}

});

// shows the box
EasyPopBox.prototype.show = function () {

	ep._show(this.id);

	if (this.media && ep._config.removeMediaOnHide)
		if (this.mediaArray)
			this.setMedia(this.mediaArray, this._mediaIndex); // resume our gallery position
		else
			this.setMedia(this.media);
	else
		this.state = ep.State.Normal;

	ep.setTopmost(this.id);

	this.reposition();
	this.element.show();

	ep._dispatch(ep.Event.Show, {
		box: this
	});

};

// hides the box
EasyPopBox.prototype.hide = function () {

	ep._hide(this.id);

	var inst = this;

	this.element.fadeOut(150, 'linear', function () {

		inst.state = ep.State.Hidden;

		if (ep._config.removeMediaOnHide && inst.media)
			inst.element.find('.ep-media').remove();

		ep._dispatch(ep.Event.Hide, {
			box: inst
		});

	});

};

// updates the window media (in-place)
EasyPopBox.prototype.setMedia = function (source, index) {

	if (source !== null && !(source instanceof EasyPopMedia) && !Array.isArray(source))
		throw new Error("EasyPop: Cannot setMedia; input must be of type EasyPopMedia");

	this.type = source ? ep.Type.Media : ep.Type.Standard;

	if (source === null)
		return;

	var isGallery = Array.isArray(source);

	if (isGallery && source && (index < 0 || index > source.length - 1))
		throw new RangeError("Index exceeds the source's range");

	this.state = ep.State.Loading;
	this.media = isGallery ? source[index || 0] : source;
	this.mediaArray = isGallery ? source : null;
	this._mediaIndex = isGallery ? index || 0 : null;

	var box = this;

	var mediaCallback = function (content) {

		// exit if we've closed - or otherwise disabled - the box
		if (box.state !== ep.State.Loading)
			return;

		// only show magnify button for images - doesn't really apply to anything else
		if (box.media.lastDeterminedType === ep.MediaType.Image && (box.magnifyMode === ep.MagnifyMode.Toggle || box.magnifyMode === ep.MagnifyMode.Forced))
			content += "<button type='button' class='ep-magnify-toggle'></button>";

		if (isGallery && box.mediaArray.length > 1) {
			content += "<span class='ep-info ep-media-count'>" + ((index || 0) + 1) + " of " + box.mediaArray.length + "</span>";
			content += "<button class='ep-gallery-arrow' data-ep-direction='left'" + (box.mediaIndex <= 0 ? ' disabled' : '') + "></button><button class='ep-gallery-arrow' data-ep-direction='right'" + (box.mediaIndex >= box.mediaArray.length - 1 ? ' disabled' : '') + "></button>";
		}

		box.title = box.media.title || box.title;
		box.content = content;

		if (box.magnifyMode === ep.MagnifyMode.Forced)
			box.magnify = true;
		else if (this._magnify)
			box.magnify = false;

		box.element.find('.ep-media').on('load canplay canplaythrough', function (e) {

			if ((e.type === 'canplaythrough' && box.media.playType !== ep.PlayType.CanPlayThrough) || (e.type === 'canplay' && box.media.playType !== ep.PlayType.Immediate))
				return;
			
			clearTimeout(ep._track.timers.loadingUpdate);

			box.element.toggleClass('ep-media-box', !!box.media);
			box.state = ep.State.Normal;

			if (box.media.lastDeterminedType === ep.MediaType.Image && this.naturalWidth && (box.media.width == null || box.media.height == null)) {
				box.media.width = this.naturalWidth;
				box.media.height = this.naturalHeight;
			}

		}).on('error', function () {

			clearTimeout(ep._track.timers.loadingUpdate);
			box.state = ep.State.Broken;

		});

	}

	this.media.getHtml(mediaCallback);

	// load progress
	ep._track.loadingHintIndex = 0;
	var loadingUpdate = function () {
		ep._track.container.find('#ep-loading').find('#ep-progress').html(ep.LoadingHints[ep._track.loadingHintIndex++]);
		if (ep._track.loadingHintIndex < ep.LoadingHints.length)
			ep._track.timers.loadingUpdate = setTimeout(loadingUpdate, 3000 * (ep._track.loadingHintIndex + 1));
	};
	ep._track.timers.loadingUpdate = setTimeout(loadingUpdate, 3000 * (ep._track.loadingHintIndex + 1));

};

// resize media and reposition window so it's central
EasyPopBox.prototype.reposition = function () {

	var width = this.media && this.media.width ? this.media.width : ep._config.defaultMediaWidth,
		height = this.media && this.media.height ? this.media.height : ep._config.defaultMediaHeight;

	if (ep._config.enforceMediaBoxMinSize && this.type === ep.Type.Media)
		this.element.attr({ 'data-ep-enforce-min-size': true }).css({ minWidth: ep._config.mediaBoxMinSize.width, minHeight: ep._config.mediaBoxMinSize.height, lineHeight: ep._config.mediaBoxMinSize.height + 'px' });

	var portWidth = $(window).innerWidth(),
		portHeight = $(window).innerHeight();

	if (ep._config.resizeMediaToPort) {

		var ratio,
			scaled = false,
			sourceWidth = width;

		if (width > portWidth * ep._config.portSpaceAvailable) {

			var newWidth = Math.floor(portWidth * ep._config.portSpaceAvailable);
			ratio = newWidth / width;

			width = newWidth;
			height = Math.round(height * ratio);

			scaled = true;

		}

		if (height > portHeight * ep._config.portSpaceAvailable) {

			var newHeight = Math.floor(portHeight * ep._config.portSpaceAvailable);
			ratio = newHeight / height;

			height = newHeight;
			width = Math.round(width * ratio);

			scaled = true;

		}

		var sourceRatio = sourceWidth / width;

		this.element.find('.ep-magnify-toggle').toggle(this.magnifyMode === ep.MagnifyMode.Forced || (scaled && this.magnifyMode === ep.MagnifyMode.Toggle && sourceRatio >= ep._config.magnifyToggleMinRatio));
	}

	var scrollY = (window.pageYOffset || document.documentElement.scrollTop) - (document.documentElement.clientTop || 0),
		scrollX = (window.pageXOffset || document.documentElement.scrollLeft) - (document.documentElement.clientLeft || 0);

	this.element.find('.ep-media').css({ width: width + 'px', height: height + 'px' });
	this.element.css({ left: 0, top: 0 });

	var outerWidth = this.element.outerWidth(),
		outerHeight = this.element.outerHeight(),
		marginX = -~~(Math.min(outerWidth, (portWidth * ep._config.portSpaceAvailable)) / 2),
		marginY = -~~(Math.min(outerHeight, (portHeight * ep._config.portSpaceAvailable)) / 2),
		top = scrollY + portHeight / 2,
		left = scrollX + portWidth / 2;

	this.element.css({ marginLeft: marginX + 'px', marginTop: marginY + 'px', top: top, left: left });

	ep._dispatch(ep.Event.Move, {
		box: this,
		width: outerWidth,
		height: outerHeight,
		left: left - Math.abs(marginX),
		top: top - Math.abs(marginY)
	});

};

// removes master reference and DOM element
EasyPopBox.prototype.destroy = function () {

	this.element.remove();

	this.state = ep.State.Destroyed;

	ep._remove(this.id);

};

// ##################################
//           EasyPopMedia
// ##################################

var EasyPopMedia = function (properties) {

	this.src = null;
	this.title = null;
	this.width = null;
	this.height = null;
	this.type = ep.MediaType.Auto;
	this.lastDeterminedType = ep.MediaType.Auto;
	this.magnifySrc = null;
	this.playType = ep.PlayType.CanPlayThrough;

	this._magnifySize = null;
	this._element = null;

	// set properties on init
	if (properties != null && typeof properties === 'object')
		for (var o in properties)
			if (properties.hasOwnProperty(o))
				if (typeof this[o] !== 'undefined') {
					if (properties[o] != null)
						this[o] = properties[o];
				} else
					console.warn("EasyPop: Invalid property (\"" + o + "\")", properties);

};

// determines the source's media type, should be treated as an asynchronous function
EasyPopMedia.prototype.determineType = function (callback, bypassHeadRequest) {

	var host = this.src.match(/^.*?\:\/\/([^\/?#]+)(?:[\/?#]|$)/i);

	if (/^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/.test(this.src))
		callback(ep.MediaType.Youtube);

	else if (/\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)/.test(this.src))
		callback(ep.MediaType.Vimeo);

	else if (/\.(jpg|jpeg|png|gif|tiff|svg|bmp)(?:$|\?)/i.test(this.src))
		callback(ep.MediaType.Image);

	else if (/\.(mp4|webm)(?:$|\?)/i.test(this.src))
		callback(ep.MediaType.Video);

	// make a head request if the config allows and we've not deferred to offline checks
	else if (ep._config.headRequestOnMedia && bypassHeadRequest !== true && (!host || ep._track.blacklistedHeadHosts.indexOf(host[1].toLowerCase()) === -1))
		var req = $.ajax({
			type: 'HEAD',
			url: this.src,
			timeout: 5000,
			success: function () {

				var type = req.getResponseHeader('Content-Type') || '';

				if (type.indexOf('image/') === 0)
					callback(ep.MediaType.Image);

				else if (type.indexOf('video/') === 0)
					callback(ep.MediaType.Video);

				else {
					console.info("EasyPop: Media has a content type of '" + type + "' (determined via HEAD) and will therefore load as a Frame");
					callback(ep.MediaType.Frame);
				}

			},
			error: function () {

				// blacklist this host for the page's lifespan, as it's likely offline or refusing CORS
				ep._track.blacklistedHeadHosts.push(host[1].toLowerCase());

				console.warn("EasyPop: Added \"" + host[1] + "\" to the HEAD request blacklist, media type check deferred to offline method");

				callback(ep.MediaType.Frame);

			}
		});

	else
		callback(ep.MediaType.Frame);

};

// gets the required HTML based on the source's determined media type, should be treated as an asynchronous function
EasyPopMedia.prototype.getHtml = function (callback) {

	var media = this;

	var typeCallback = function (type) {

		media.lastDeterminedType = type;

		var parts;

		switch (type) {

			case ep.MediaType.Image:
				callback("<img src='" + media.src + "' class='ep-media' alt='Failed' />");
				break;

			case ep.MediaType.Video:
				callback("<video class='ep-media' controls autoplay><source src='" + media.src + "' type='video/" + (media.src.indexOf('.webm') !== -1 ? 'webm' : 'mp4') + "'></video>");
				break;

			case ep.MediaType.Youtube:
				parts = media.src.match(/^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/);
				callback("<iframe class='ep-media' src='https://www.youtube.com/embed/" + parts[2] + "?" + $.param(ep._config.youtubeParameters) + "' allowfullscreen />");
				break;

			case ep.MediaType.Vimeo:
				parts = media.src.match(/\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)/);
				callback("<iframe class='ep-media' src='https://player.vimeo.com/video/" + parts[3] + "?" + $.param(ep._config.vimeoParameters) + "' allowfullscreen />");
				break;

			default:
				callback("<iframe class='ep-media' src='" + media.src + "' />");
				break;

		}

	}

	if (this.type === ep.MediaType.Auto)
		this.determineType(typeCallback);
	else
		typeCallback(this.type);

};

// ##################################
//             page ready
// ##################################

$(function() {

	// ##################################
	//         jQuery extension
	// ##################################

	$.fn.ep = function (params) {

		params = typeof params !== 'undefined' ? params : {};

		return this.each(function () {

			var trumpId = params.id || $(this).attr('data-ep-id') || this.id || null;

			params.content = params.clone === true ? $(this).clone() : $(this); // if clone is false (default), the target element will be moved to EP's box, rather than duplicated
			params.overwrite = true;

			ep.create(trumpId, params);

		});

	};

	// ##################################
	//          helper bindings
	// ##################################

	$('body').on('click', '.ep', function (e) {

		e.preventDefault();

		var box;

		// show an existing box
		if ($(this).attr('data-ep-show')) {

			box = ep.get($(this).attr('data-ep-show'));

			if (box)
				box.show();
			else
				console.warn("EasyPop: No box found with an id of '" + $(this).attr('data-ep-show') + "'");

		}
		// create a new media box
		else {

			var invoker = this,
				items = $(this),
				group = $(this).closest('.ep-group');

			if (group.length > 0)
				items = group.find('.ep');

			var media = [],
				trackIndex = 0,
				initialIndex = 0;

			items.each(function () {

				var trumpSrc = $(this).attr('data-ep') || $(this).attr('src') || $(this).attr('href');

				if (trumpSrc) {

					media.push(new EasyPopMedia({
						src: trumpSrc,
						width: Number($(this).attr('data-ep-width')) || null,
						height: Number($(this).attr('data-ep-height')) || null,
						type: $(this).attr('data-ep-type') || null,
						title: $(this).attr('data-ep-title') || null,
						magnifySrc: $(this).attr('data-ep-magnify-src') || null,
						playType: $(this).attr('data-ep-play-type') || ep.PlayType.CanPlayThrough
					}));

					if (this === invoker)
						initialIndex = trackIndex;

					trackIndex++; // can't rely on .each index, due to some items being skipped below

				}
				else
					console.warn("EasyPop: Skipping media item '" + (this.id || this.className) + "'; no acceptable source attribute found");

			});

			box = ep.create(null);
			box.setMedia(media, initialIndex);

		}

	});

});